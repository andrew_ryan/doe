doe v1.1.52 (D:\CODE\gitlab_code\doe)
├── aes v0.7.5
│   ├── cfg-if v1.0.0
│   ├── cipher v0.3.0
│   │   └── generic-array v0.14.7
│   │       └── typenum v1.17.0
│   │       [build-dependencies]
│   │       └── version_check v0.9.5
│   ├── cpufeatures v0.2.14
│   └── opaque-debug v0.3.1
├── anyhow v1.0.95
├── axum v0.7.9
│   ├── async-trait v0.1.85 (proc-macro)
│   │   ├── proc-macro2 v1.0.86
│   │   │   └── unicode-ident v1.0.12
│   │   ├── quote v1.0.37
│   │   │   └── proc-macro2 v1.0.86 (*)
│   │   └── syn v2.0.87
│   │       ├── proc-macro2 v1.0.86 (*)
│   │       ├── quote v1.0.37 (*)
│   │       └── unicode-ident v1.0.12
│   ├── axum-core v0.4.5
│   │   ├── async-trait v0.1.85 (proc-macro) (*)
│   │   ├── bytes v1.7.1
│   │   ├── futures-util v0.3.30
│   │   │   ├── futures-core v0.3.30
│   │   │   ├── futures-io v0.3.30
│   │   │   ├── futures-macro v0.3.30 (proc-macro)
│   │   │   │   ├── proc-macro2 v1.0.86 (*)
│   │   │   │   ├── quote v1.0.37 (*)
│   │   │   │   └── syn v2.0.87 (*)
│   │   │   ├── futures-sink v0.3.30
│   │   │   ├── futures-task v0.3.30
│   │   │   ├── memchr v2.7.4
│   │   │   ├── pin-project-lite v0.2.14
│   │   │   ├── pin-utils v0.1.0
│   │   │   └── slab v0.4.9
│   │   │       [build-dependencies]
│   │   │       └── autocfg v1.3.0
│   │   ├── http v1.2.0
│   │   │   ├── bytes v1.7.1
│   │   │   ├── fnv v1.0.7
│   │   │   └── itoa v1.0.11
│   │   ├── http-body v1.0.1
│   │   │   ├── bytes v1.7.1
│   │   │   └── http v1.2.0 (*)
│   │   ├── http-body-util v0.1.2
│   │   │   ├── bytes v1.7.1
│   │   │   ├── futures-util v0.3.30 (*)
│   │   │   ├── http v1.2.0 (*)
│   │   │   ├── http-body v1.0.1 (*)
│   │   │   └── pin-project-lite v0.2.14
│   │   ├── mime v0.3.17
│   │   ├── pin-project-lite v0.2.14
│   │   ├── rustversion v1.0.19 (proc-macro)
│   │   ├── sync_wrapper v1.0.2
│   │   ├── tower-layer v0.3.3
│   │   ├── tower-service v0.3.3
│   │   └── tracing v0.1.40
│   │       ├── log v0.4.22
│   │       │   └── value-bag v1.10.0
│   │       ├── pin-project-lite v0.2.14
│   │       ├── tracing-attributes v0.1.27 (proc-macro)
│   │       │   ├── proc-macro2 v1.0.86 (*)
│   │       │   ├── quote v1.0.37 (*)
│   │       │   └── syn v2.0.87 (*)
│   │       └── tracing-core v0.1.32
│   │           └── once_cell v1.19.0
│   ├── axum-macros v0.4.2 (proc-macro)
│   │   ├── proc-macro2 v1.0.86 (*)
│   │   ├── quote v1.0.37 (*)
│   │   └── syn v2.0.87 (*)
│   ├── base64 v0.22.1
│   ├── bytes v1.7.1
│   ├── futures-util v0.3.30 (*)
│   ├── http v1.2.0 (*)
│   ├── http-body v1.0.1 (*)
│   ├── http-body-util v0.1.2 (*)
│   ├── hyper v1.5.2
│   │   ├── bytes v1.7.1
│   │   ├── futures-channel v0.3.30
│   │   │   └── futures-core v0.3.30
│   │   ├── futures-util v0.3.30 (*)
│   │   ├── h2 v0.4.7
│   │   │   ├── atomic-waker v1.1.2
│   │   │   ├── bytes v1.7.1
│   │   │   ├── fnv v1.0.7
│   │   │   ├── futures-core v0.3.30
│   │   │   ├── futures-sink v0.3.30
│   │   │   ├── http v1.2.0 (*)
│   │   │   ├── indexmap v2.5.0
│   │   │   │   ├── equivalent v1.0.1
│   │   │   │   └── hashbrown v0.14.5
│   │   │   │       ├── ahash v0.8.11
│   │   │   │       │   ├── cfg-if v1.0.0
│   │   │   │       │   ├── getrandom v0.2.15
│   │   │   │       │   │   └── cfg-if v1.0.0
│   │   │   │       │   ├── once_cell v1.19.0
│   │   │   │       │   └── zerocopy v0.7.35
│   │   │   │       │       ├── byteorder v1.5.0
│   │   │   │       │       └── zerocopy-derive v0.7.35 (proc-macro)
│   │   │   │       │           ├── proc-macro2 v1.0.86 (*)
│   │   │   │       │           ├── quote v1.0.37 (*)
│   │   │   │       │           └── syn v2.0.87 (*)
│   │   │   │       │   [build-dependencies]
│   │   │   │       │   └── version_check v0.9.5
│   │   │   │       └── allocator-api2 v0.2.18
│   │   │   ├── slab v0.4.9 (*)
│   │   │   ├── tokio v1.40.0
│   │   │   │   ├── bytes v1.7.1
│   │   │   │   ├── mio v1.0.2
│   │   │   │   │   └── windows-sys v0.52.0
│   │   │   │   │       └── windows-targets v0.52.6
│   │   │   │   │           └── windows_x86_64_msvc v0.52.6
│   │   │   │   ├── parking_lot v0.12.3
│   │   │   │   │   ├── lock_api v0.4.12
│   │   │   │   │   │   └── scopeguard v1.2.0
│   │   │   │   │   │   [build-dependencies]
│   │   │   │   │   │   └── autocfg v1.3.0
│   │   │   │   │   └── parking_lot_core v0.9.10
│   │   │   │   │       ├── cfg-if v1.0.0
│   │   │   │   │       ├── smallvec v1.13.2
│   │   │   │   │       └── windows-targets v0.52.6 (*)
│   │   │   │   ├── pin-project-lite v0.2.14
│   │   │   │   ├── socket2 v0.5.7
│   │   │   │   │   └── windows-sys v0.52.0 (*)
│   │   │   │   ├── tokio-macros v2.4.0 (proc-macro)
│   │   │   │   │   ├── proc-macro2 v1.0.86 (*)
│   │   │   │   │   ├── quote v1.0.37 (*)
│   │   │   │   │   └── syn v2.0.87 (*)
│   │   │   │   └── windows-sys v0.52.0 (*)
│   │   │   ├── tokio-util v0.7.12
│   │   │   │   ├── bytes v1.7.1
│   │   │   │   ├── futures-core v0.3.30
│   │   │   │   ├── futures-io v0.3.30
│   │   │   │   ├── futures-sink v0.3.30
│   │   │   │   ├── pin-project-lite v0.2.14
│   │   │   │   └── tokio v1.40.0 (*)
│   │   │   └── tracing v0.1.40 (*)
│   │   ├── http v1.2.0 (*)
│   │   ├── http-body v1.0.1 (*)
│   │   ├── httparse v1.9.4
│   │   ├── httpdate v1.0.3
│   │   ├── itoa v1.0.11
│   │   ├── pin-project-lite v0.2.14
│   │   ├── smallvec v1.13.2
│   │   ├── tokio v1.40.0 (*)
│   │   └── want v0.3.1
│   │       └── try-lock v0.2.5
│   ├── hyper-util v0.1.10
│   │   ├── bytes v1.7.1
│   │   ├── futures-util v0.3.30 (*)
│   │   ├── http v1.2.0 (*)
│   │   ├── http-body v1.0.1 (*)
│   │   ├── hyper v1.5.2 (*)
│   │   ├── pin-project-lite v0.2.14
│   │   ├── tokio v1.40.0 (*)
│   │   └── tower-service v0.3.3
│   ├── itoa v1.0.11
│   ├── matchit v0.7.3
│   ├── memchr v2.7.4
│   ├── mime v0.3.17
│   ├── percent-encoding v2.3.1
│   ├── pin-project-lite v0.2.14
│   ├── rustversion v1.0.19 (proc-macro)
│   ├── serde v1.0.210
│   │   └── serde_derive v1.0.210 (proc-macro)
│   │       ├── proc-macro2 v1.0.86 (*)
│   │       ├── quote v1.0.37 (*)
│   │       └── syn v2.0.87 (*)
│   ├── serde_json v1.0.128
│   │   ├── itoa v1.0.11
│   │   ├── memchr v2.7.4
│   │   ├── ryu v1.0.18
│   │   └── serde v1.0.210 (*)
│   ├── serde_path_to_error v0.1.16
│   │   ├── itoa v1.0.11
│   │   └── serde v1.0.210 (*)
│   ├── serde_urlencoded v0.7.1
│   │   ├── form_urlencoded v1.2.1
│   │   │   └── percent-encoding v2.3.1
│   │   ├── itoa v1.0.11
│   │   ├── ryu v1.0.18
│   │   └── serde v1.0.210 (*)
│   ├── sha1 v0.10.6
│   │   ├── cfg-if v1.0.0
│   │   ├── cpufeatures v0.2.14
│   │   └── digest v0.10.7
│   │       ├── block-buffer v0.10.4
│   │       │   └── generic-array v0.14.7 (*)
│   │       ├── const-oid v0.9.6
│   │       ├── crypto-common v0.1.6
│   │       │   ├── generic-array v0.14.7 (*)
│   │       │   └── typenum v1.17.0
│   │       └── subtle v2.6.1
│   ├── sync_wrapper v1.0.2
│   ├── tokio v1.40.0 (*)
│   ├── tokio-tungstenite v0.24.0
│   │   ├── futures-util v0.3.30 (*)
│   │   ├── log v0.4.22 (*)
│   │   ├── tokio v1.40.0 (*)
│   │   └── tungstenite v0.24.0
│   │       ├── byteorder v1.5.0
│   │       ├── bytes v1.7.1
│   │       ├── data-encoding v2.7.0
│   │       ├── http v1.2.0 (*)
│   │       ├── httparse v1.9.4
│   │       ├── log v0.4.22 (*)
│   │       ├── rand v0.8.5
│   │       │   ├── rand_chacha v0.3.1
│   │       │   │   ├── ppv-lite86 v0.2.20
│   │       │   │   │   └── zerocopy v0.7.35 (*)
│   │       │   │   └── rand_core v0.6.4
│   │       │   │       └── getrandom v0.2.15 (*)
│   │       │   └── rand_core v0.6.4 (*)
│   │       ├── sha1 v0.10.6 (*)
│   │       ├── thiserror v1.0.63
│   │       │   └── thiserror-impl v1.0.63 (proc-macro)
│   │       │       ├── proc-macro2 v1.0.86 (*)
│   │       │       ├── quote v1.0.37 (*)
│   │       │       └── syn v2.0.87 (*)
│   │       └── utf-8 v0.7.6
│   ├── tower v0.5.2
│   │   ├── futures-core v0.3.30
│   │   ├── futures-util v0.3.30 (*)
│   │   ├── pin-project-lite v0.2.14
│   │   ├── sync_wrapper v1.0.2
│   │   ├── tokio v1.40.0 (*)
│   │   ├── tower-layer v0.3.3
│   │   ├── tower-service v0.3.3
│   │   └── tracing v0.1.40 (*)
│   ├── tower-layer v0.3.3
│   ├── tower-service v0.3.3
│   └── tracing v0.1.40 (*)
├── base64 v0.21.7
├── blake3 v1.5.4
│   ├── arrayref v0.3.8
│   ├── arrayvec v0.7.6
│   ├── cfg-if v1.0.0
│   └── constant_time_eq v0.3.1
│   [build-dependencies]
│   └── cc v1.1.18
│       ├── jobserver v0.1.32
│       └── shlex v1.3.0
├── block-modes v0.8.1
│   ├── block-padding v0.2.1
│   └── cipher v0.3.0 (*)
├── chrono v0.4.38
│   ├── num-traits v0.2.19
│   │   └── libm v0.2.8
│   │   [build-dependencies]
│   │   └── autocfg v1.3.0
│   └── windows-targets v0.52.6 (*)
├── clipboard v0.5.0
│   └── clipboard-win v2.2.0
│       └── winapi v0.3.9
├── clipboard-win v5.4.0
│   └── error-code v3.2.0
├── crossterm v0.27.0
│   ├── bitflags v2.6.0
│   ├── crossterm_winapi v0.9.1
│   │   └── winapi v0.3.9
│   ├── parking_lot v0.12.3 (*)
│   └── winapi v0.3.9
├── ctr v0.8.0
│   └── cipher v0.3.0 (*)
├── deadpool-tiberius v0.1.8
│   ├── deadpool v0.10.0
│   │   ├── async-trait v0.1.85 (proc-macro) (*)
│   │   ├── deadpool-runtime v0.1.4
│   │   │   └── tokio v1.40.0 (*)
│   │   ├── num_cpus v1.16.0
│   │   └── tokio v1.40.0 (*)
│   ├── thiserror v1.0.63 (*)
│   ├── tiberius v0.12.3
│   │   ├── async-io v1.13.0
│   │   │   ├── async-lock v2.8.0
│   │   │   │   └── event-listener v2.5.3
│   │   │   ├── cfg-if v1.0.0
│   │   │   ├── concurrent-queue v2.5.0
│   │   │   │   └── crossbeam-utils v0.8.20
│   │   │   ├── futures-lite v1.13.0
│   │   │   │   ├── fastrand v1.9.0
│   │   │   │   ├── futures-core v0.3.30
│   │   │   │   ├── futures-io v0.3.30
│   │   │   │   ├── memchr v2.7.4
│   │   │   │   ├── parking v2.2.1
│   │   │   │   ├── pin-project-lite v0.2.14
│   │   │   │   └── waker-fn v1.2.0
│   │   │   ├── log v0.4.22 (*)
│   │   │   ├── parking v2.2.1
│   │   │   ├── polling v2.8.0
│   │   │   │   ├── bitflags v1.3.2
│   │   │   │   ├── cfg-if v1.0.0
│   │   │   │   ├── concurrent-queue v2.5.0 (*)
│   │   │   │   ├── log v0.4.22 (*)
│   │   │   │   ├── pin-project-lite v0.2.14
│   │   │   │   └── windows-sys v0.48.0
│   │   │   │       └── windows-targets v0.48.5
│   │   │   │           └── windows_x86_64_msvc v0.48.5
│   │   │   │   [build-dependencies]
│   │   │   │   └── autocfg v1.3.0
│   │   │   ├── rustix v0.37.28
│   │   │   │   ├── bitflags v1.3.2
│   │   │   │   ├── errno v0.3.9
│   │   │   │   │   └── windows-sys v0.52.0 (*)
│   │   │   │   ├── io-lifetimes v1.0.11
│   │   │   │   │   └── windows-sys v0.48.0 (*)
│   │   │   │   └── windows-sys v0.48.0 (*)
│   │   │   ├── slab v0.4.9 (*)
│   │   │   ├── socket2 v0.4.10
│   │   │   │   └── winapi v0.3.9
│   │   │   └── waker-fn v1.2.0
│   │   │   [build-dependencies]
│   │   │   └── autocfg v1.3.0
│   │   ├── async-native-tls v0.4.0
│   │   │   ├── futures-util v0.3.30 (*)
│   │   │   ├── native-tls v0.2.12
│   │   │   │   └── schannel v0.1.24
│   │   │   │       └── windows-sys v0.59.0
│   │   │   │           └── windows-targets v0.52.6 (*)
│   │   │   ├── thiserror v1.0.63 (*)
│   │   │   └── url v2.5.2
│   │   │       ├── form_urlencoded v1.2.1 (*)
│   │   │       ├── idna v0.5.0
│   │   │       │   ├── unicode-bidi v0.3.15
│   │   │       │   └── unicode-normalization v0.1.23
│   │   │       │       └── tinyvec v1.8.0
│   │   │       │           └── tinyvec_macros v0.1.1
│   │   │       └── percent-encoding v2.3.1
│   │   ├── async-net v1.8.0
│   │   │   ├── async-io v1.13.0 (*)
│   │   │   ├── blocking v1.6.1
│   │   │   │   ├── async-channel v2.3.1
│   │   │   │   │   ├── concurrent-queue v2.5.0 (*)
│   │   │   │   │   ├── event-listener-strategy v0.5.3
│   │   │   │   │   │   ├── event-listener v5.4.0
│   │   │   │   │   │   │   ├── concurrent-queue v2.5.0 (*)
│   │   │   │   │   │   │   ├── parking v2.2.1
│   │   │   │   │   │   │   └── pin-project-lite v0.2.14
│   │   │   │   │   │   └── pin-project-lite v0.2.14
│   │   │   │   │   ├── futures-core v0.3.30
│   │   │   │   │   └── pin-project-lite v0.2.14
│   │   │   │   ├── async-task v4.7.1
│   │   │   │   ├── futures-io v0.3.30
│   │   │   │   ├── futures-lite v2.6.0
│   │   │   │   │   ├── fastrand v2.1.1
│   │   │   │   │   ├── futures-core v0.3.30
│   │   │   │   │   ├── futures-io v0.3.30
│   │   │   │   │   ├── parking v2.2.1
│   │   │   │   │   └── pin-project-lite v0.2.14
│   │   │   │   └── piper v0.2.4
│   │   │   │       ├── atomic-waker v1.1.2
│   │   │   │       ├── fastrand v2.1.1
│   │   │   │       └── futures-io v0.3.30
│   │   │   └── futures-lite v1.13.0 (*)
│   │   ├── async-std v1.13.0
│   │   │   ├── async-attributes v1.1.2 (proc-macro)
│   │   │   │   ├── quote v1.0.37 (*)
│   │   │   │   └── syn v1.0.109
│   │   │   │       ├── proc-macro2 v1.0.86 (*)
│   │   │   │       ├── quote v1.0.37 (*)
│   │   │   │       └── unicode-ident v1.0.12
│   │   │   ├── async-channel v1.9.0
│   │   │   │   ├── concurrent-queue v2.5.0 (*)
│   │   │   │   ├── event-listener v2.5.3
│   │   │   │   └── futures-core v0.3.30
│   │   │   ├── async-global-executor v2.4.1
│   │   │   │   ├── async-channel v2.3.1 (*)
│   │   │   │   ├── async-executor v1.13.1
│   │   │   │   │   ├── async-task v4.7.1
│   │   │   │   │   ├── concurrent-queue v2.5.0 (*)
│   │   │   │   │   ├── fastrand v2.1.1
│   │   │   │   │   ├── futures-lite v2.6.0 (*)
│   │   │   │   │   └── slab v0.4.9 (*)
│   │   │   │   ├── async-io v2.4.0
│   │   │   │   │   ├── async-lock v3.4.0
│   │   │   │   │   │   ├── event-listener v5.4.0 (*)
│   │   │   │   │   │   ├── event-listener-strategy v0.5.3 (*)
│   │   │   │   │   │   └── pin-project-lite v0.2.14
│   │   │   │   │   ├── cfg-if v1.0.0
│   │   │   │   │   ├── concurrent-queue v2.5.0 (*)
│   │   │   │   │   ├── futures-io v0.3.30
│   │   │   │   │   ├── futures-lite v2.6.0 (*)
│   │   │   │   │   ├── parking v2.2.1
│   │   │   │   │   ├── polling v3.7.4
│   │   │   │   │   │   ├── cfg-if v1.0.0
│   │   │   │   │   │   ├── concurrent-queue v2.5.0 (*)
│   │   │   │   │   │   ├── pin-project-lite v0.2.14
│   │   │   │   │   │   ├── tracing v0.1.40 (*)
│   │   │   │   │   │   └── windows-sys v0.59.0 (*)
│   │   │   │   │   ├── rustix v0.38.36
│   │   │   │   │   │   ├── bitflags v2.6.0
│   │   │   │   │   │   ├── errno v0.3.9 (*)
│   │   │   │   │   │   └── windows-sys v0.52.0 (*)
│   │   │   │   │   ├── slab v0.4.9 (*)
│   │   │   │   │   ├── tracing v0.1.40 (*)
│   │   │   │   │   └── windows-sys v0.59.0 (*)
│   │   │   │   ├── async-lock v3.4.0 (*)
│   │   │   │   ├── blocking v1.6.1 (*)
│   │   │   │   ├── futures-lite v2.6.0 (*)
│   │   │   │   └── once_cell v1.19.0
│   │   │   ├── async-io v2.4.0 (*)
│   │   │   ├── async-lock v3.4.0 (*)
│   │   │   ├── crossbeam-utils v0.8.20
│   │   │   ├── futures-core v0.3.30
│   │   │   ├── futures-io v0.3.30
│   │   │   ├── futures-lite v2.6.0 (*)
│   │   │   ├── kv-log-macro v1.0.7
│   │   │   │   └── log v0.4.22 (*)
│   │   │   ├── log v0.4.22 (*)
│   │   │   ├── memchr v2.7.4
│   │   │   ├── once_cell v1.19.0
│   │   │   ├── pin-project-lite v0.2.14
│   │   │   ├── pin-utils v0.1.0
│   │   │   └── slab v0.4.9 (*)
│   │   ├── async-trait v0.1.85 (proc-macro) (*)
│   │   ├── asynchronous-codec v0.6.2
│   │   │   ├── bytes v1.7.1
│   │   │   ├── futures-sink v0.3.30
│   │   │   ├── futures-util v0.3.30 (*)
│   │   │   ├── memchr v2.7.4
│   │   │   └── pin-project-lite v0.2.14
│   │   ├── bigdecimal v0.3.1
│   │   │   ├── num-bigint v0.4.6
│   │   │   │   ├── num-integer v0.1.46
│   │   │   │   │   └── num-traits v0.2.19 (*)
│   │   │   │   └── num-traits v0.2.19 (*)
│   │   │   ├── num-integer v0.1.46 (*)
│   │   │   └── num-traits v0.2.19 (*)
│   │   ├── byteorder v1.5.0
│   │   ├── bytes v1.7.1
│   │   ├── chrono v0.4.38 (*)
│   │   ├── connection-string v0.2.0
│   │   ├── encoding_rs v0.8.34
│   │   │   └── cfg-if v1.0.0
│   │   ├── enumflags2 v0.7.11
│   │   │   └── enumflags2_derive v0.7.11 (proc-macro)
│   │   │       ├── proc-macro2 v1.0.86 (*)
│   │   │       ├── quote v1.0.37 (*)
│   │   │       └── syn v2.0.87 (*)
│   │   ├── futures-lite v1.13.0 (*)
│   │   ├── futures-util v0.3.30 (*)
│   │   ├── num-traits v0.2.19 (*)
│   │   ├── once_cell v1.19.0
│   │   ├── pin-project-lite v0.2.14
│   │   ├── pretty-hex v0.3.0
│   │   ├── rust_decimal v1.36.0
│   │   │   ├── arrayvec v0.7.6
│   │   │   ├── num-traits v0.2.19 (*)
│   │   │   └── serde v1.0.210 (*)
│   │   ├── thiserror v1.0.63 (*)
│   │   ├── time v0.3.37
│   │   │   ├── deranged v0.3.11
│   │   │   │   └── powerfmt v0.2.0
│   │   │   ├── itoa v1.0.11
│   │   │   ├── num-conv v0.1.0
│   │   │   ├── powerfmt v0.2.0
│   │   │   └── time-core v0.1.2
│   │   ├── tokio v1.40.0 (*)
│   │   ├── tokio-util v0.7.12 (*)
│   │   ├── tracing v0.1.40 (*)
│   │   ├── uuid v1.10.0
│   │   └── winauth v0.0.4
│   │       ├── bitflags v1.3.2
│   │       ├── byteorder v1.5.0
│   │       ├── md5 v0.6.1
│   │       ├── rand v0.7.3
│   │       │   ├── getrandom v0.1.16
│   │       │   │   └── cfg-if v1.0.0
│   │       │   ├── rand_chacha v0.2.2
│   │       │   │   ├── ppv-lite86 v0.2.20 (*)
│   │       │   │   └── rand_core v0.5.1
│   │       │   │       └── getrandom v0.1.16 (*)
│   │       │   └── rand_core v0.5.1 (*)
│   │       └── winapi v0.3.9
│   ├── tokio v1.40.0 (*)
│   └── tokio-util v0.7.12 (*)
├── directories v5.0.1
│   └── dirs-sys v0.4.1
│       ├── option-ext v0.2.0
│       └── windows-sys v0.48.0 (*)
├── futures-util v0.3.30 (*)
├── hex v0.4.3
├── http v1.2.0 (*)
├── hyper v1.5.2 (*)
├── icns v0.3.1
│   ├── byteorder v1.5.0
│   └── png v0.16.8
│       ├── bitflags v1.3.2
│       ├── crc32fast v1.4.2
│       │   └── cfg-if v1.0.0
│       ├── deflate v0.8.6
│       │   ├── adler32 v1.2.0
│       │   └── byteorder v1.5.0
│       └── miniz_oxide v0.3.7
│           └── adler32 v1.2.0
├── ico v0.3.0
│   ├── byteorder v1.5.0
│   └── png v0.17.13
│       ├── bitflags v1.3.2
│       ├── crc32fast v1.4.2 (*)
│       ├── fdeflate v0.3.4
│       │   └── simd-adler32 v0.3.7
│       ├── flate2 v1.0.33
│       │   ├── crc32fast v1.4.2 (*)
│       │   └── miniz_oxide v0.8.0
│       │       └── adler2 v2.0.0
│       └── miniz_oxide v0.7.4
│           ├── adler v1.0.2
│           └── simd-adler32 v0.3.7
├── if-addrs v0.13.3
│   └── windows-sys v0.52.0 (*)
├── image v0.24.9
│   ├── bytemuck v1.18.0
│   ├── byteorder v1.5.0
│   ├── color_quant v1.1.0
│   ├── exr v1.72.0
│   │   ├── bit_field v0.10.2
│   │   ├── flume v0.11.0
│   │   │   └── spin v0.9.8
│   │   │       └── lock_api v0.4.12 (*)
│   │   ├── half v2.4.1
│   │   │   └── cfg-if v1.0.0
│   │   ├── lebe v0.5.2
│   │   ├── miniz_oxide v0.7.4 (*)
│   │   ├── rayon-core v1.12.1
│   │   │   ├── crossbeam-deque v0.8.5
│   │   │   │   ├── crossbeam-epoch v0.9.18
│   │   │   │   │   └── crossbeam-utils v0.8.20
│   │   │   │   └── crossbeam-utils v0.8.20
│   │   │   └── crossbeam-utils v0.8.20
│   │   ├── smallvec v1.13.2
│   │   └── zune-inflate v0.2.54
│   │       └── simd-adler32 v0.3.7
│   ├── gif v0.13.1
│   │   ├── color_quant v1.1.0
│   │   └── weezl v0.1.8
│   ├── jpeg-decoder v0.3.1
│   │   └── rayon v1.10.0
│   │       ├── either v1.13.0
│   │       └── rayon-core v1.12.1 (*)
│   ├── num-traits v0.2.19 (*)
│   ├── png v0.17.13 (*)
│   ├── qoi v0.4.1
│   │   └── bytemuck v1.18.0
│   └── tiff v0.9.1
│       ├── flate2 v1.0.33 (*)
│       ├── jpeg-decoder v0.3.1 (*)
│       └── weezl v0.1.8
├── lazy_static v1.5.0
│   └── spin v0.9.8 (*)
├── md5 v0.7.0
├── mouse-rs v0.4.2
│   ├── libloading v0.6.7
│   │   └── winapi v0.3.9
│   └── winapi v0.3.9
├── percent-encoding v2.3.1
├── pinyin v0.10.0
├── rand v0.8.5 (*)
├── regex v1.10.6
│   ├── aho-corasick v1.1.3
│   │   └── memchr v2.7.4
│   ├── memchr v2.7.4
│   ├── regex-automata v0.4.7
│   │   ├── aho-corasick v1.1.3 (*)
│   │   ├── memchr v2.7.4
│   │   └── regex-syntax v0.8.4
│   └── regex-syntax v0.8.4
├── reqwest v0.11.27
│   ├── base64 v0.21.7
│   ├── bytes v1.7.1
│   ├── encoding_rs v0.8.34 (*)
│   ├── futures-core v0.3.30
│   ├── futures-util v0.3.30 (*)
│   ├── h2 v0.3.26
│   │   ├── bytes v1.7.1
│   │   ├── fnv v1.0.7
│   │   ├── futures-core v0.3.30
│   │   ├── futures-sink v0.3.30
│   │   ├── futures-util v0.3.30 (*)
│   │   ├── http v0.2.12
│   │   │   ├── bytes v1.7.1
│   │   │   ├── fnv v1.0.7
│   │   │   └── itoa v1.0.11
│   │   ├── indexmap v2.5.0 (*)
│   │   ├── slab v0.4.9 (*)
│   │   ├── tokio v1.40.0 (*)
│   │   ├── tokio-util v0.7.12 (*)
│   │   └── tracing v0.1.40 (*)
│   ├── http v0.2.12 (*)
│   ├── http-body v0.4.6
│   │   ├── bytes v1.7.1
│   │   ├── http v0.2.12 (*)
│   │   └── pin-project-lite v0.2.14
│   ├── hyper v0.14.30
│   │   ├── bytes v1.7.1
│   │   ├── futures-channel v0.3.30 (*)
│   │   ├── futures-core v0.3.30
│   │   ├── futures-util v0.3.30 (*)
│   │   ├── h2 v0.3.26 (*)
│   │   ├── http v0.2.12 (*)
│   │   ├── http-body v0.4.6 (*)
│   │   ├── httparse v1.9.4
│   │   ├── httpdate v1.0.3
│   │   ├── itoa v1.0.11
│   │   ├── pin-project-lite v0.2.14
│   │   ├── socket2 v0.5.7 (*)
│   │   ├── tokio v1.40.0 (*)
│   │   ├── tower-service v0.3.3
│   │   ├── tracing v0.1.40 (*)
│   │   └── want v0.3.1 (*)
│   ├── hyper-tls v0.5.0
│   │   ├── bytes v1.7.1
│   │   ├── hyper v0.14.30 (*)
│   │   ├── native-tls v0.2.12 (*)
│   │   ├── tokio v1.40.0 (*)
│   │   └── tokio-native-tls v0.3.1
│   │       ├── native-tls v0.2.12 (*)
│   │       └── tokio v1.40.0 (*)
│   ├── ipnet v2.9.0
│   ├── log v0.4.22 (*)
│   ├── mime v0.3.17
│   ├── native-tls v0.2.12 (*)
│   ├── once_cell v1.19.0
│   ├── percent-encoding v2.3.1
│   ├── pin-project-lite v0.2.14
│   ├── rustls-pemfile v1.0.4
│   │   └── base64 v0.21.7
│   ├── serde v1.0.210 (*)
│   ├── serde_json v1.0.128 (*)
│   ├── serde_urlencoded v0.7.1 (*)
│   ├── sync_wrapper v0.1.2
│   ├── tokio v1.40.0 (*)
│   ├── tokio-native-tls v0.3.1 (*)
│   ├── tower-service v0.3.3
│   ├── url v2.5.2 (*)
│   └── winreg v0.50.0
│       ├── cfg-if v1.0.0
│       └── windows-sys v0.48.0 (*)
├── rsa v0.9.6
│   ├── const-oid v0.9.6
│   ├── digest v0.10.7 (*)
│   ├── num-bigint-dig v0.8.4
│   │   ├── byteorder v1.5.0
│   │   ├── lazy_static v1.5.0 (*)
│   │   ├── libm v0.2.8
│   │   ├── num-integer v0.1.46 (*)
│   │   ├── num-iter v0.1.45
│   │   │   ├── num-integer v0.1.46 (*)
│   │   │   └── num-traits v0.2.19 (*)
│   │   │   [build-dependencies]
│   │   │   └── autocfg v1.3.0
│   │   ├── num-traits v0.2.19 (*)
│   │   ├── rand v0.8.5 (*)
│   │   ├── smallvec v1.13.2
│   │   └── zeroize v1.8.1
│   ├── num-integer v0.1.46 (*)
│   ├── num-traits v0.2.19 (*)
│   ├── pkcs1 v0.7.5
│   │   ├── der v0.7.9
│   │   │   ├── const-oid v0.9.6
│   │   │   ├── pem-rfc7468 v0.7.0
│   │   │   │   └── base64ct v1.6.0
│   │   │   └── zeroize v1.8.1
│   │   ├── pkcs8 v0.10.2
│   │   │   ├── der v0.7.9 (*)
│   │   │   └── spki v0.7.3
│   │   │       └── der v0.7.9 (*)
│   │   └── spki v0.7.3 (*)
│   ├── pkcs8 v0.10.2 (*)
│   ├── rand_core v0.6.4 (*)
│   ├── signature v2.2.0
│   │   ├── digest v0.10.7 (*)
│   │   └── rand_core v0.6.4 (*)
│   ├── spki v0.7.3 (*)
│   ├── subtle v2.6.1
│   └── zeroize v1.8.1
├── scraper v0.17.1
│   ├── ahash v0.8.11 (*)
│   ├── cssparser v0.31.2
│   │   ├── cssparser-macros v0.6.1 (proc-macro)
│   │   │   ├── quote v1.0.37 (*)
│   │   │   └── syn v2.0.87 (*)
│   │   ├── dtoa-short v0.3.5
│   │   │   └── dtoa v1.0.9
│   │   ├── itoa v1.0.11
│   │   ├── phf v0.11.2
│   │   │   ├── phf_macros v0.11.2 (proc-macro)
│   │   │   │   ├── phf_generator v0.11.2
│   │   │   │   │   ├── phf_shared v0.11.2
│   │   │   │   │   │   └── siphasher v0.3.11
│   │   │   │   │   └── rand v0.8.5
│   │   │   │   │       ├── rand_chacha v0.3.1 (*)
│   │   │   │   │       └── rand_core v0.6.4 (*)
│   │   │   │   ├── phf_shared v0.11.2 (*)
│   │   │   │   ├── proc-macro2 v1.0.86 (*)
│   │   │   │   ├── quote v1.0.37 (*)
│   │   │   │   └── syn v2.0.87 (*)
│   │   │   └── phf_shared v0.11.2
│   │   │       └── siphasher v0.3.11
│   │   └── smallvec v1.13.2
│   ├── ego-tree v0.6.3
│   ├── getopts v0.2.21
│   │   └── unicode-width v0.1.13
│   ├── html5ever v0.26.0
│   │   ├── log v0.4.22 (*)
│   │   ├── mac v0.1.1
│   │   └── markup5ever v0.11.0
│   │       ├── log v0.4.22 (*)
│   │       ├── phf v0.10.1
│   │       │   └── phf_shared v0.10.0
│   │       │       └── siphasher v0.3.11
│   │       ├── string_cache v0.8.7
│   │       │   ├── new_debug_unreachable v1.0.6
│   │       │   ├── once_cell v1.19.0
│   │       │   ├── parking_lot v0.12.3 (*)
│   │       │   ├── phf_shared v0.10.0 (*)
│   │       │   ├── precomputed-hash v0.1.1
│   │       │   └── serde v1.0.210 (*)
│   │       └── tendril v0.4.3
│   │           ├── futf v0.1.5
│   │           │   ├── mac v0.1.1
│   │           │   └── new_debug_unreachable v1.0.6
│   │           ├── mac v0.1.1
│   │           └── utf-8 v0.7.6
│   │       [build-dependencies]
│   │       ├── phf_codegen v0.10.0
│   │       │   ├── phf_generator v0.10.0
│   │       │   │   ├── phf_shared v0.10.0 (*)
│   │       │   │   └── rand v0.8.5 (*)
│   │       │   └── phf_shared v0.10.0 (*)
│   │       └── string_cache_codegen v0.5.2
│   │           ├── phf_generator v0.10.0 (*)
│   │           ├── phf_shared v0.10.0 (*)
│   │           ├── proc-macro2 v1.0.86 (*)
│   │           └── quote v1.0.37 (*)
│   │   [build-dependencies]
│   │   ├── proc-macro2 v1.0.86 (*)
│   │   ├── quote v1.0.37 (*)
│   │   └── syn v1.0.109 (*)
│   ├── once_cell v1.19.0
│   ├── selectors v0.25.0
│   │   ├── bitflags v2.6.0
│   │   ├── cssparser v0.31.2 (*)
│   │   ├── derive_more v0.99.18 (proc-macro)
│   │   │   ├── proc-macro2 v1.0.86 (*)
│   │   │   ├── quote v1.0.37 (*)
│   │   │   └── syn v2.0.87 (*)
│   │   ├── fxhash v0.2.1
│   │   │   └── byteorder v1.5.0
│   │   ├── log v0.4.22 (*)
│   │   ├── new_debug_unreachable v1.0.6
│   │   ├── phf v0.10.1 (*)
│   │   ├── precomputed-hash v0.1.1
│   │   ├── servo_arc v0.3.0
│   │   │   └── stable_deref_trait v1.2.0
│   │   └── smallvec v1.13.2
│   │   [build-dependencies]
│   │   └── phf_codegen v0.10.0 (*)
│   ├── smallvec v1.13.2
│   └── tendril v0.4.3 (*)
├── screenshots v0.8.10
│   ├── anyhow v1.0.95
│   ├── display-info v0.4.8
│   │   ├── anyhow v1.0.95
│   │   ├── fxhash v0.2.1 (*)
│   │   ├── widestring v1.1.0
│   │   └── windows v0.52.0
│   │       ├── windows-core v0.52.0
│   │       │   └── windows-targets v0.52.6 (*)
│   │       └── windows-targets v0.52.6 (*)
│   ├── fxhash v0.2.1 (*)
│   ├── image v0.24.9 (*)
│   ├── percent-encoding v2.3.1
│   ├── widestring v1.1.0
│   └── windows v0.51.1
│       ├── windows-core v0.51.1
│       │   └── windows-targets v0.48.5 (*)
│       └── windows-targets v0.48.5 (*)
├── serde_json v1.0.128 (*)
├── sha1 v0.10.6 (*)
├── sha2 v0.10.8
│   ├── cfg-if v1.0.0
│   ├── cpufeatures v0.2.14
│   └── digest v0.10.7 (*)
├── sha3 v0.10.8
│   ├── digest v0.10.7 (*)
│   └── keccak v0.1.5
├── socket2 v0.5.7 (*)
├── time v0.3.37 (*)
├── tokio v1.40.0 (*)
├── tower v0.4.13
│   ├── futures-core v0.3.30
│   ├── futures-util v0.3.30 (*)
│   ├── pin-project v1.1.8
│   │   └── pin-project-internal v1.1.8 (proc-macro)
│   │       ├── proc-macro2 v1.0.86 (*)
│   │       ├── quote v1.0.37 (*)
│   │       └── syn v2.0.87 (*)
│   ├── pin-project-lite v0.2.14
│   ├── tokio v1.40.0 (*)
│   ├── tokio-util v0.7.12 (*)
│   ├── tower-layer v0.3.3
│   ├── tower-service v0.3.3
│   └── tracing v0.1.40 (*)
├── tower-http v0.5.2
│   ├── base64 v0.21.7
│   ├── bitflags v2.6.0
│   ├── bytes v1.7.1
│   ├── http v1.2.0 (*)
│   ├── http-body v1.0.1 (*)
│   ├── http-body-util v0.1.2 (*)
│   ├── mime v0.3.17
│   ├── pin-project-lite v0.2.14
│   ├── tower-layer v0.3.3
│   ├── tower-service v0.3.3
│   └── tracing v0.1.40 (*)
├── tracing v0.1.40 (*)
├── tracing-appender v0.2.3
│   ├── crossbeam-channel v0.5.13
│   │   └── crossbeam-utils v0.8.20
│   ├── thiserror v1.0.63 (*)
│   ├── time v0.3.37 (*)
│   └── tracing-subscriber v0.3.18
│       ├── chrono v0.4.38 (*)
│       ├── matchers v0.1.0
│       │   └── regex-automata v0.1.10
│       │       └── regex-syntax v0.6.29
│       ├── nu-ansi-term v0.46.0
│       │   ├── overload v0.1.1
│       │   └── winapi v0.3.9
│       ├── once_cell v1.19.0
│       ├── regex v1.10.6 (*)
│       ├── sharded-slab v0.1.7
│       │   └── lazy_static v1.5.0 (*)
│       ├── smallvec v1.13.2
│       ├── thread_local v1.1.8
│       │   ├── cfg-if v1.0.0
│       │   └── once_cell v1.19.0
│       ├── time v0.3.37 (*)
│       ├── tracing v0.1.40 (*)
│       ├── tracing-core v0.1.32 (*)
│       └── tracing-log v0.2.0
│           ├── log v0.4.22 (*)
│           ├── once_cell v1.19.0
│           └── tracing-core v0.1.32 (*)
├── tracing-error v0.2.0
│   ├── tracing v0.1.40 (*)
│   └── tracing-subscriber v0.3.18 (*)
├── tracing-subscriber v0.3.18 (*)
├── umya-spreadsheet v1.2.7
│   ├── aes v0.8.4
│   │   ├── cfg-if v1.0.0
│   │   ├── cipher v0.4.4
│   │   │   ├── crypto-common v0.1.6 (*)
│   │   │   └── inout v0.1.3
│   │   │       ├── block-padding v0.3.3
│   │   │       │   └── generic-array v0.14.7 (*)
│   │   │       └── generic-array v0.14.7 (*)
│   │   └── cpufeatures v0.2.14
│   ├── ahash v0.8.11 (*)
│   ├── base64 v0.21.7
│   ├── byteorder v1.5.0
│   ├── cbc v0.1.2
│   │   └── cipher v0.4.4 (*)
│   ├── cfb v0.9.0
│   │   ├── byteorder v1.5.0
│   │   ├── fnv v1.0.7
│   │   └── uuid v1.10.0
│   ├── chrono v0.4.38 (*)
│   ├── encoding_rs v0.8.34 (*)
│   ├── fancy-regex v0.13.0
│   │   ├── bit-set v0.5.3
│   │   │   └── bit-vec v0.6.3
│   │   ├── regex-automata v0.4.7 (*)
│   │   └── regex-syntax v0.8.4
│   ├── getrandom v0.2.15 (*)
│   ├── hashbrown v0.14.5 (*)
│   ├── hmac v0.12.1
│   │   └── digest v0.10.7 (*)
│   ├── html_parser v0.7.0
│   │   ├── doc-comment v0.3.3
│   │   ├── pest v2.7.12
│   │   │   ├── memchr v2.7.4
│   │   │   ├── thiserror v1.0.63 (*)
│   │   │   └── ucd-trie v0.1.6
│   │   ├── pest_derive v2.7.12 (proc-macro)
│   │   │   ├── pest v2.7.12 (*)
│   │   │   └── pest_generator v2.7.12
│   │   │       ├── pest v2.7.12 (*)
│   │   │       ├── pest_meta v2.7.12
│   │   │       │   ├── once_cell v1.19.0
│   │   │       │   └── pest v2.7.12 (*)
│   │   │       │   [build-dependencies]
│   │   │       │   └── sha2 v0.10.8
│   │   │       │       ├── cfg-if v1.0.0
│   │   │       │       ├── cpufeatures v0.2.14
│   │   │       │       └── digest v0.10.7
│   │   │       │           ├── block-buffer v0.10.4 (*)
│   │   │       │           └── crypto-common v0.1.6
│   │   │       │               ├── generic-array v0.14.7 (*)
│   │   │       │               └── typenum v1.17.0
│   │   │       ├── proc-macro2 v1.0.86 (*)
│   │   │       ├── quote v1.0.37 (*)
│   │   │       └── syn v2.0.87 (*)
│   │   ├── serde v1.0.210 (*)
│   │   ├── serde_derive v1.0.210 (proc-macro) (*)
│   │   ├── serde_json v1.0.128 (*)
│   │   └── thiserror v1.0.63 (*)
│   ├── image v0.24.9 (*)
│   ├── lazy_static v1.5.0 (*)
│   ├── md-5 v0.10.6
│   │   ├── cfg-if v1.0.0
│   │   └── digest v0.10.7 (*)
│   ├── quick-xml v0.31.0
│   │   ├── memchr v2.7.4
│   │   └── serde v1.0.210 (*)
│   ├── regex v1.10.6 (*)
│   ├── sha2 v0.10.8 (*)
│   ├── thousands v0.2.0
│   └── zip v0.6.6
│       ├── aes v0.8.4 (*)
│       ├── byteorder v1.5.0
│       ├── bzip2 v0.4.4
│       │   ├── bzip2-sys v0.1.11+1.0.8
│       │   │   └── libc v0.2.158
│       │   │   [build-dependencies]
│       │   │   ├── cc v1.1.18 (*)
│       │   │   └── pkg-config v0.3.30
│       │   └── libc v0.2.158
│       ├── constant_time_eq v0.1.5
│       ├── crc32fast v1.4.2 (*)
│       ├── flate2 v1.0.33 (*)
│       ├── hmac v0.12.1 (*)
│       ├── pbkdf2 v0.11.0
│       │   ├── digest v0.10.7 (*)
│       │   ├── hmac v0.12.1 (*)
│       │   ├── password-hash v0.4.2
│       │   │   ├── base64ct v1.6.0
│       │   │   ├── rand_core v0.6.4 (*)
│       │   │   └── subtle v2.6.1
│       │   └── sha2 v0.10.8 (*)
│       ├── sha1 v0.10.6 (*)
│       ├── time v0.3.37 (*)
│       └── zstd v0.11.2+zstd.1.5.2
│           └── zstd-safe v5.0.2+zstd.1.5.2
│               ├── libc v0.2.158
│               └── zstd-sys v2.0.13+zstd.1.5.6
│                   [build-dependencies]
│                   ├── cc v1.1.18 (*)
│                   └── pkg-config v0.3.30
├── unicode-segmentation v1.11.0
├── winapi v0.3.9
├── xdotool v0.0.2
├── xml-rs v0.8.21
└── zip v0.6.6 (*)