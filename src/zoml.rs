#[allow(warnings)]
pub mod zoml {
    ///The zoml_to_csv function takes a value that implements the ToString trait, converts it to a string using the to_string method, then splits the string on newline characters (\n) to create an iterator of lines. For each line, it splits the string on double colon characters (::) to create an iterator of fields, converts each field to a String, and collects them into a vector. Finally, it collects all the vectors of fields into a single vector of rows, which is returned as the result of the function.
    pub fn zoml_to_csv(zoml: impl ToString) -> Vec<Vec<String>> {
        let zoml = zoml.to_string();
        zoml.split("\n")
            .map(|s| {
                s.split("::")
                    .map(|s| s.to_string())
                    .collect::<Vec<String>>()
            })
            .collect::<Vec<Vec<String>>>()
    }
    ///The csv_to_zoml function takes a vector of vectors of strings (representing a CSV table), iterates over the rows, joins each row with a double colon character (::) to form a string, collects all the strings into a vector, and finally joins the vector with newline characters (\n) to form a single string, which is returned as the result of the function.Overall, this library provides useful tools for working with data in both the ZOML and CSV formats.
    pub fn csv_to_zoml(csv: Vec<Vec<String>>) -> String {
        csv.iter()
            .map(|row| row.join("::"))
            .collect::<Vec<String>>()
            .join("\n")
    }
}
