#[allow(warnings)]
#[cfg(feature = "xlsx")]
pub mod xlsx {
    use std::path::PathBuf;

    use crate::{DebugPrint, Print};
    ///
    /// value_type can be 'formula' 'string' 'number' 'bool' 'hyperlink'
    ///
    ///```rust
    ///use doe::*;
    ///xlsx::xlsx_set_values("./demo.xlsx", "Sheet1", &[cellvalue!("M4", "3", "number")]);
    /// ```
    ///
    #[macro_export]
    macro_rules! cellvalue {
        ($coordinate:expr,$value:expr,$value_type:expr) => {
            $crate::xlsx::CellValue::new($coordinate, $value, $value_type)
        };
    }
    ///num_to_col
    /// ```ignore
    ///col_to_num("D").dprintln();//4
    ///num_to_col(4).dprintln();//D
    ///coordinate_to_position("D27").dprintln();//4,27
    ///position_to_coordinate(4, 27).unwrap().dprintln();//D27
    /// ```
    pub fn num_to_col(index: usize) -> String {
        let index = index + 1;
        let mut result = String::new();
        let mut index = index;

        while index > 0 {
            let remainder = (index - 1) % 26;
            result.push((b'A' + remainder as u8) as char);
            index = (index - 1) / 26;
        }

        result.chars().rev().collect()
    }
    ///col_to_num
    /// ```ignore
    ///col_to_num("D").dprintln();//4
    ///num_to_col(4).dprintln();//D
    ///coordinate_to_position("D27").dprintln();//4,27
    ///position_to_coordinate(4, 27).unwrap().dprintln();//D27
    /// ```
    pub fn col_to_num(col: impl ToString) -> usize {
        let col_str = col.to_string().to_uppercase();
        let mut col_num = 0;
        for (i, c) in col_str.chars().enumerate() {
            if (c as u8 as i8 - 'A' as u8 as i8) >= 0 {
                let offset = (c as u8 - b'A') as usize + 1;
                col_num = col_num * 26 + offset;
            }
        }
        col_num - 1
    }
    ///coordinate_to_position
    /// ```ignore
    ///col_to_num("D").dprintln();//4
    ///num_to_col(4).dprintln();//D
    ///coordinate_to_position("D27").dprintln();//4,27
    ///position_to_coordinate(4, 27).unwrap().dprintln();//D27
    /// ```
    pub fn coordinate_to_position(coordinate: impl ToString) -> (usize, usize) {
        let coord_str = coordinate.to_string();
        let mut col_str = String::new();
        let mut row_str = String::new();
        for c in coord_str.chars() {
            if c.is_digit(10) {
                row_str.push(c);
            } else {
                col_str.push(c);
            }
        }
        let col_num = col_to_num(col_str);
        let row_num = row_str.parse::<usize>().unwrap();
        (col_num, row_num)
    }
    ///position_to_coordinate
    /// ```ignore
    ///col_to_num("D").dprintln();//4
    ///num_to_col(4).dprintln();//D
    ///coordinate_to_position("D27").dprintln();//4,27
    ///position_to_coordinate(4, 27).unwrap().dprintln();//D27
    /// ```
    pub fn position_to_coordinate(x: usize, y: usize) -> Option<String> {
        if x >= 1 && y >= 1 {
            let mut num = x;
            let mut col_name = String::new();
            while num > 0 {
                let rem = (num - 1) % 26;
                col_name.insert(0, ((rem as u8) + b'A') as char);
                num = (num - 1) / 26;
            }
            Some(format!("{}{}", col_name, y))
        } else {
            None
        }
    }
    ///xlsx_get_sheet_names
    ///```ignore
    ///let sheet_names = xlsx_get_sheet_names("./book.xlsx").unwrap();
    ///println!("{:?}", sheet_names);
    ///```
    pub fn xlsx_get_sheet_names(xlsx_path: impl ToString) -> Option<Vec<String>> {
        let xlsx_path = xlsx_path.to_string();
        let path = std::path::Path::new(&xlsx_path);
        if let std::result::Result::Ok(mut xlsx_book) =
            umya_spreadsheet::reader::xlsx::lazy_read(path)
        {
            let sheet_count = xlsx_book.get_sheet_count();
            let sheet_names: Vec<String> = xlsx_book
                .get_sheet_collection_no_check()
                .iter()
                .map(|s| s.get_name().to_string())
                .collect();
            return Some(sheet_names);
        } else {
            panic!("Unexpected error at read xlsx file");
            return None;
        }
        return None;
    }
    ///xlsx_get_cell_value
    ///```ignore
    /// let cell_value = xlsx_get_cell_value("./lang.xlsx","Sheet1","D27");
    /// ```
    pub fn xlsx_get_cell_value(
        xlsx_path: impl ToString,
        sheet_name: impl ToString,
        coordinate: impl ToString,
    ) -> Option<String> {
        let xlsx_path = xlsx_path.to_string();
        let sheet_name = sheet_name.to_string();
        let coordinate = coordinate.to_string();
        let path = std::path::Path::new(&xlsx_path);
        if let std::result::Result::Ok(mut xlsx_book) =
            umya_spreadsheet::reader::xlsx::lazy_read(path)
        {
            if let Some(sheet) = xlsx_book.get_sheet_by_name_mut(&sheet_name) {
                let value = sheet.get_cell_value(coordinate).get_value().to_string();
                return Some(value);
            } else {
                panic!("Unexpected error at get sheet");
            }
            return None;
        } else {
            panic!("Unexpected error at read xlsx file");
        }
        return None;
    }

    pub fn xlsx_get_cell(
        xlsx_path: impl ToString,
        sheet_name: impl ToString,
        coordinate: impl ToString,
    ) -> Option<umya_spreadsheet::CellValue> {
        let xlsx_path = xlsx_path.to_string();
        let sheet_name = sheet_name.to_string();
        let coordinate = coordinate.to_string();
        let path = std::path::Path::new(&xlsx_path);
        if let std::result::Result::Ok(mut xlsx_book) =
            umya_spreadsheet::reader::xlsx::lazy_read(path)
        {
            if let Some(sheet) = xlsx_book.get_sheet_by_name_mut(&sheet_name) {
                let value = sheet.get_cell_value(coordinate);
                return Some(value.to_owned());
            } else {
                panic!("Unexpected error at get sheet");
            }
            return None;
        } else {
            panic!("Unexpected error at read xlsx file");
        }
        return None;
    }
    /// CellValue coordinate is linke "A4" ..
    ///
    /// value is impl Tosting
    ///
    /// value_type can be 'formula' 'string' 'number' 'bool' 'hyperlink'
    pub struct CellValue<T: ToString, U: ToString> {
        pub coordinate: T,
        pub value: U,
        pub value_type: &'static str,
    }
    ///
    ///```rust
    ///use doe::*;
    ///xlsx::xlsx_set_values("./demo.xlsx", "Sheet1", &[CellValue::new("M4", "", "formula_attributes")]);
    /// ```
    ///
    impl<T: ToString, U: ToString> CellValue<T, U> {
        ///
        ///```rust
        ///use doe::*;
        ///xlsx::xlsx_set_values("./demo.xlsx", "Sheet1", &[CellValue::new("M4", "", "formula_attributes")]);
        /// ```
        /// value_type can be 'formula' 'string' 'number' 'bool' 'hyperlink'
        ///
        pub fn new(coordinate: T, value: U, value_type: &'static str) -> Self {
            Self {
                coordinate,
                value,
                value_type,
            }
        }
    }
    use umya_spreadsheet::Hyperlink;
    ///```rust
    /// use doe::*;
    /// xlsx::xlsx_set_values("./rust.xlsx", "Sheet1", &[cellvalue!("A5","rust","string")]);
    /// xlsx::xlsx_set_values("./demo.xlsx", "Sheet1", &[CellValue::new("B4", "./XJTJWSHRD2023000026.pdf,pdf", "hyperlink")]).unwrap();
    ///  let mut cellvalues = vec![];
    ///  for (index, c) in c_vec.iter().enumerate() {
    ///      for (a, q) in a_vec.iter().zip(q_vec.iter()) {
    ///          if c == q{
    ///              cellvalues.push(cellvalue!("A".push_back(index+2), a, "string"));
    ///          }
    ///          
    ///     }
    /// }
    /// xlsx_set_values("148.xlsx", "1704344947003", &cellvalues).unwrap();
    /// ```
    pub fn xlsx_set_values(
        xlsx_path: impl ToString,
        sheet_name: impl ToString,
        values: &[CellValue<impl ToString, impl ToString>],
    ) -> Result<(), Box<dyn std::error::Error>> {
        let values = values;
        use umya_spreadsheet::writer;
        let xlsx_path = xlsx_path.to_string();
        let path = std::path::Path::new(&xlsx_path);
        let mut xlsx_book =
            umya_spreadsheet::reader::xlsx::lazy_read(path).expect("read xlsx Error");
        let mut sheet = xlsx_book
            .get_sheet_by_name_mut(&sheet_name.to_string())
            .expect("read sheet Error");
        for cellvalue in values.iter() {
            let mut cell = sheet.get_cell_mut(cellvalue.coordinate.to_string());
            let mut cell_value = cell.get_cell_value_mut();
            if cellvalue.value_type.to_string() == "string" {
                cell_value.set_value_string(cellvalue.value.to_string());
            } else if cellvalue.value_type.to_string() == "number" {
                cell_value.set_value_number(
                    cellvalue
                        .value
                        .to_string()
                        .parse::<f64>()
                        .expect("number parse f64 Error"),
                );
            } else if cellvalue.value_type.to_string() == "formula" {
                cell_value.set_formula(cellvalue.value.to_string());
            }
            // fn main() {
            //     use doe::*;
            //     xlsx::xlsx_set_values("./demo.xlsx", "Sheet1", &[CellValue::new("B4", "./XJTJWSHRD2023000026.pdf,pdf", "hyperlink")]).unwrap();
            // }
            else if cellvalue.value_type.to_string() == "hyperlink" {
                let mut hyperlink = Hyperlink::default();
                let v = cellvalue.value.to_string();
                let hyperlink_vec: Vec<_> = v.split(",").filter(|s| !s.is_empty()).collect();
                hyperlink
                    .set_url(hyperlink_vec.iter().nth(0).unwrap().to_string())
                    .set_tooltip(hyperlink_vec.iter().nth(1).unwrap().to_string())
                    .set_location(false);

                cell.set_hyperlink(hyperlink);
            } else if cellvalue.value_type.to_string() == "bool" {
                let value = move || {
                    if cellvalue.value.to_string() == "true" || cellvalue.value.to_string() == "1" {
                        true
                    } else if cellvalue.value.to_string() == "false"
                        || cellvalue.value.to_string() == "0"
                    {
                        false
                    } else {
                        false
                    }
                };
                cell_value.set_value_bool(value());
            }
        }
        let _ = writer::xlsx::write(&xlsx_book, path);
        std::result::Result::Ok(())
    }
    ///xlsx_set_cell_value
    /// ```ignore
    /// use doe::xlsx::position_to_coordinate as ptc;
    /// use doe::*;
    /// xlsx::xlsx_get_sheet_names("./book.xlsx").dprintln();
    /// for x in 1..10 {
    ///     for y in 1..10 {
    ///         xlsx::xlsx_set_cell_value_string("./book.xlsx", "Info", ptc(x, y).unwrap(), format!("{},{}", x, y));
    ///     }
    /// }
    /// xlsx::xlsx_set_cell_value_string("./book.xlsx", "Sheet1", "D27", "some value").unwrap();
    /// ```
    ///
    pub fn xlsx_set_cell_value_string(
        xlsx_path: impl ToString,
        sheet_name: impl ToString,
        coordinate: impl ToString,
        new_value: impl ToString,
    ) -> Result<(), Box<dyn std::error::Error>> {
        use umya_spreadsheet::writer;
        let xlsx_path = xlsx_path.to_string();
        let path = std::path::Path::new(&xlsx_path);
        if let std::result::Result::Ok(mut xlsx_book) =
            umya_spreadsheet::reader::xlsx::lazy_read(path)
        {
            if let Some(mut sheet) = xlsx_book.get_sheet_by_name_mut(&sheet_name.to_string()) {
                let mut cell = sheet.get_cell_value_mut(coordinate.to_string());
                cell.set_value_string(new_value.to_string());
                let _ = writer::xlsx::write(&xlsx_book, path);
            } else {
                panic!("Unexpected error at get sheet");
            }
        } else {
            panic!("Unexpected error at read xlsx file");
        }
        std::result::Result::Ok(())
    }
    ///
    /// ```ignore
    /// let data:Vec<String> = xlsx_get_col_values("2024-01-08.xlsx", "检验报告查询导出2024-01-08","J").unwrap();
    /// ```
    ///
    ///
    pub fn xlsx_get_col_values(
        xlsx_path: impl ToString,
        sheet_name: impl ToString,
        col: impl ToString,
    ) -> Result<Vec<String>, Box<dyn std::error::Error>> {
        let xlsx_path = xlsx_path.to_string();
        let sheet_name = sheet_name.to_string();
        let col = col.to_string();
        let col_num = col_to_num(col);
        let path = std::path::Path::new(&xlsx_path);
        let mut xlsx_book = umya_spreadsheet::reader::xlsx::lazy_read(path)?;
        let mut sheet = xlsx_book
            .get_sheet_by_name_mut(&sheet_name.to_string())
            .expect("Couldn't find sheet");
        let cells = sheet.get_cell_collection();
        let mut max = 0;
        for cell in cells.clone() {
            let coordinate = cell.get_coordinate().get_coordinate();
            let (x, y) = coordinate_to_position(coordinate);
            if x > max {
                max = x;
            }
            if y > max {
                max = y;
            }
        }
        let mut csv_data = vec![];
        for cell in cells.clone() {
            let coordinate = cell.get_coordinate().get_coordinate();
            let (x, y) = coordinate_to_position(coordinate);
            // csv_data[y - 1][x - 1] = cell.get_value().to_string();
            if x == col_num {
                csv_data.push(cell.get_value().to_string());
            }
        }
        return std::result::Result::Ok(csv_data);
    }
    pub fn xlsx_to_csv(
        xlsx_path: impl ToString,
        sheet_name: impl ToString,
    ) -> Result<Vec<Vec<String>>, Box<dyn std::error::Error>> {
        let xlsx_path = xlsx_path.to_string();
        let sheet_name = sheet_name.to_string();
        let path = std::path::Path::new(&xlsx_path);
        let mut xlsx_book = umya_spreadsheet::reader::xlsx::lazy_read(path)?;
        let mut sheet = xlsx_book
            .get_sheet_by_name_mut(&sheet_name.to_string())
            .expect("get_sheet_by_name error");
        let sheet_clone = sheet.clone();
        let col = sheet_clone.get_highest_column();
        let row = sheet_clone.get_highest_row();
        let mut csv_data = vec![];
        for r in 1..row + 1 {
            let mut row_vec = Vec::new();
            for c in 1..col + 1 {
                let mut cell = sheet.get_cell_value_mut((c as u32, r as u32));
                row_vec.push(cell.get_value_lazy().to_string());
            }
            csv_data.push(row_vec);
        }
        return std::result::Result::Ok(csv_data);
    }
    ///xlsx_to_btree_map
    /// ```ignore
    /// use doe::*;
    /// let bmap = doe::xlsx::xlsx_to_btree_map("get_xlsx.xlsx")?;
    /// bmap.iter().for_each(|(k, v)| {
    ///     std::fs::write(k.push_back(".csv"), v.iter().map(|s|s.join(",")).collect::<Vec<_>>().join("\n")).unwrap();
    /// });
    /// ```
    pub fn xlsx_to_btree_map(
        path: &str,
    ) -> crate::DynError<std::collections::BTreeMap<String, Vec<Vec<String>>>> {
        // 每个sheet就是一个二维数组
        use crate::*;
        // key 是sheet的名字
        // value 是二维数组
        let mut btree_map: std::collections::BTreeMap<String, Vec<Vec<String>>> = btreemap!();
        // 读xlsx
        if let Some(sheet_names) = crate::xlsx::xlsx_get_sheet_names(path) {
            for sheet_name in sheet_names {
                if let std::result::Result::Ok(sheet_data) =
                    crate::xlsx::xlsx_to_csv(path, sheet_name.clone())
                {
                    btree_map.insert(sheet_name, sheet_data);
                }
            }
        }
        std::result::Result::Ok(btree_map)
    }
    pub fn xlsx_to_csv_and_write(
        xlsx_path: impl ToString,
        sheet_name: impl ToString,
        csv_path: impl ToString,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let csv_path = csv_path.to_string();
        let csv: Vec<Vec<String>> = xlsx_to_csv(xlsx_path, sheet_name).unwrap();
        let mut csv_string: String = csv
            .iter()
            .map(|s| s.join(","))
            .collect::<Vec<_>>()
            .join("\n");
        std::fs::write(csv_path, csv_string).unwrap();
        std::result::Result::Ok(())
    }
    /// xlsx replace values and save new xlsx file
    /// ```ignore
    ///use doe::xlsx::*;
    /// //xlsx replace [A] to andrew in the xlsx file
    ///let _ = xlsx_replace_values_save("demo.xlsx".into(), vec![("[A]".into(),"andrew".into())], "new_demo.xlsx".into()).unwrap();
    ///````
    ///
    pub fn xlsx_replace_values_save<T>(
        xlsx_path: PathBuf,
        values: Vec<(T, T)>,
        new_xlsx_path: PathBuf,
    ) -> Result<(), Box<dyn std::error::Error>>
    where
        T: ToString,
    {
        use std::fs::File;
        use std::io::prelude::*;
        use zip::read::ZipArchive;
        use zip::write::FileOptions;
        use zip::CompressionMethod;

        // Open the .docx file as a zip
        let file = File::open(xlsx_path.clone())?;
        let mut archive = ZipArchive::new(file)?;
        let new_archive_path = "new_archive.zip";

        let options = FileOptions::default()
            .compression_method(CompressionMethod::Stored)
            .unix_permissions(0o755);

        let file = File::create(&new_archive_path).unwrap();
        let mut new_zip = zip::ZipWriter::new(file);

        // Loop over all of the files in the .docx archive
        for i in 0..archive.len() {
            let mut file_in_archive = archive.by_index(i).unwrap();
            let file_in_archive_name = &file_in_archive.name().to_string();
            if file_in_archive_name.to_string().ends_with(".xml") {
                let mut contents = String::new();
                file_in_archive.read_to_string(&mut contents).unwrap();
                let mut new_contents = contents.to_string();
                for value in &values {
                    let (target, new_target) = value;
                    // Perform text replacement
                    new_contents =
                        new_contents.replace(&target.to_string(), &new_target.to_string());
                }
                new_zip.start_file(file_in_archive_name, options)?;
                new_zip.write_all(new_contents.as_bytes())?;
            } else {
                new_zip.start_file(file_in_archive_name.to_string(), options)?;
                let mut buffer = Vec::new();
                file_in_archive.read_to_end(&mut buffer)?;
                new_zip.write_all(&buffer)?;
            }
        }
        std::fs::rename(new_archive_path, new_xlsx_path).unwrap();
        std::result::Result::Ok(())
    }

    use anyhow::*; // 导入anyhow库，用于错误处理
                   // use doe::{
                   //     // 导入doe库中的相关模块
                   //     Str,                                                              // 导入Str类型
                   //     xlsx::{xlsx_get_sheet_names, xlsx_to_csv, xlsx_to_csv_and_write}, // 导入xlsx模块中的函数
                   // };
    use std::{collections::BTreeMap, path::Path}; // 导入Path类型，用于处理文件路径

    // 以下代码片段导入了多个库和模块，主要用于处理Excel文件（xlsx格式）的读取和转换操作。
    // 具体功能包括获取Excel文件中的工作表名称、将Excel文件转换为CSV格式，以及将转换后的CSV数据写入文件。
    // 这些操作依赖于`doe`库中的`xlsx`模块，并且使用了`anyhow`库进行错误处理。

    /// 将CSV格式的数据转换为Markdown表格格式的字符串。
    ///
    /// # 参数
    /// - `csv`: 一个二维字符串向量，表示CSV数据。每一行是一个字符串向量，表示CSV的一行数据。
    ///
    /// # 返回值
    /// 返回一个字符串，表示转换后的Markdown表格。如果输入的CSV数据为空，则返回空字符串。
    pub fn csv_to_markdown(csv: &Vec<Vec<String>>) -> String {
        // 如果CSV数据为空，直接返回空字符串
        if csv.is_empty() {
            return String::new();
        }

        // 获取CSV的表头行
        let header_row = &csv[0];
        // 生成Markdown表格的分隔行，格式为 "|---|...|---|"
        let separator = format!("|{}|", vec!["---"; header_row.len()].join("|"));
        let mut md_table = Vec::new();

        // 添加表头行到Markdown表格中
        md_table.push(format!("| {} |", header_row.join(" | ")));

        // 添加分隔行到Markdown表格中
        md_table.push(separator);

        // 遍历CSV数据行（跳过表头行），并将每一行添加到Markdown表格中
        for data_row in csv.iter().skip(1) {
            md_table.push(format!("| {} |", data_row.join(" | ")));
        }

        // 将Markdown表格的每一行用换行符连接，形成最终的Markdown表格字符串
        md_table.join("\n")
    }

    /// 将 XLSX 文件转换为 Markdown 格式的字符串列表。
    ///
    /// 该函数读取指定的 XLSX 文件，并将其中的每个工作表转换为 Markdown 格式的字符串。
    /// 每个工作表的内容将被转换为一个 Markdown 字符串，并返回包含所有工作表 Markdown 字符串的 `BTreeMap`。
    ///
    /// # 参数
    /// - `xlsx_path`: XLSX 文件的路径，可以是任何实现了 `AsRef<Path>` 的类型。
    ///
    /// # 返回值
    /// - 返回 `Result<BTreeMap<String, String>>`，如果转换成功则返回 `Ok`，其中包含一个 `BTreeMap`：
    ///   - 键为工作表的名称。
    ///   - 值为对应工作表的 Markdown 格式字符串。
    /// - 如果转换过程中出现错误，则返回包含错误信息的 `Err`。
    pub fn xlsx_to_markdown(xlsx_path: impl AsRef<Path>) -> Result<BTreeMap<String, String>> {
        // 获取 XLSX 文件中的所有工作表名称
        let sheet_names = xlsx_get_sheet_names(xlsx_path.as_ref().to_string_lossy())
            .context("Failed to get sheet names from XLSX file")?;
        let mut res = BTreeMap::new();

        // 遍历每个工作表，将其转换为 Markdown 格式
        for sheet_name in sheet_names.iter() {
            // 将当前工作表转换为 CSV 格式的二维向量
            let csv: Vec<Vec<String>> =
                xlsx_to_csv(xlsx_path.as_ref().to_string_lossy(), sheet_name.to_string())
                    .map_err(|s| anyhow!(s.to_string()))?
                    .into_iter()
                    .map(|s| {
                        s.iter()
                            .map(|s| {
                                if s.is_empty() {
                                    // 如果单元格为空，则用 "-" 代替
                                    "-".to_string()
                                } else {
                                    // 去除单元格内容的前后空白字符
                                    s.trim().to_string()
                                }
                            })
                            .collect()
                    })
                    .filter(|s: &Vec<String>| {
                        let ss = s.clone().join("").replace("-", "");
                        !ss.is_empty()
                    })
                    .collect();

            // 将 CSV 格式的数据转换为 Markdown 格式的字符串
            let markdown_string = csv_to_markdown(&csv);

            // 将工作表名称和对应的 Markdown 字符串插入到结果映射中
            res.insert(sheet_name.to_string(), markdown_string);
        }

        // 转换成功，返回包含 Markdown 字符串和工作表名称的映射
        Ok(res)
    }

    /// 将 XLSX 文件转换为 Markdown 文件。
    ///
    /// 该函数读取指定的 XLSX 文件，并将其中的每个工作表转换为 Markdown 格式的文件。
    /// 每个工作表将生成一个对应的 `.md` 文件，文件名为工作表的名称。
    ///
    /// # 参数
    /// - `xlsx_path`: XLSX 文件的路径，可以是任何实现了 `AsRef<Path>` 的类型。
    /// - `output_path`: 输出 Markdown 文件的路径，可以是任何实现了 `AsRef<Path>` 的类型。
    ///
    /// # 返回值
    /// - 返回 `Result<()>`，如果转换成功则返回 `Ok(())`，否则返回包含错误信息的 `Err`。
    pub fn xlsx_to_markdown_write(
        xlsx_path: impl AsRef<Path>,
        output_path: impl AsRef<Path>,
    ) -> Result<()> {
        // 获取 XLSX 文件中的所有工作表名称
        let sheet_names = xlsx_get_sheet_names(xlsx_path.as_ref().to_string_lossy())
            .context("Failed to get sheet names from XLSX file")?;

        // 遍历每个工作表，将其转换为 Markdown 格式并保存为 `.md` 文件
        for sheet_name in sheet_names {
            // 将当前工作表转换为 CSV 格式的二维向量
            let csv: Vec<Vec<String>> =
                xlsx_to_csv(xlsx_path.as_ref().to_string_lossy(), sheet_name.to_string())
                    .map_err(|s| anyhow!(s.to_string()))?
                    .into_iter()
                    .map(|s| {
                        s.iter()
                            .map(|s| {
                                if s.is_empty() {
                                    // 如果单元格为空，则用 "-" 代替
                                    "-".to_string()
                                } else {
                                    // 去除单元格内容的前后空白字符
                                    s.trim().to_string()
                                }
                            })
                            .collect()
                    })
                    .filter(|s: &Vec<String>| {
                        let ss = s.clone().join("").replace("-", "");
                        !ss.is_empty()
                    })
                    .collect();

            // 将 CSV 格式的数据转换为 Markdown 格式的字符串
            let csv_string = csv_to_markdown(&csv);

            // 将 Markdown 字符串写入以工作表名称命名的 `.md` 文件
            if !output_path.as_ref().to_path_buf().exists() {
                std::fs::create_dir(output_path.as_ref().to_path_buf())?;
            }
            use crate::traits::traits::Str;
            let path = output_path
                .as_ref()
                .to_path_buf()
                .join(sheet_name.to_string().push_back(".md"));
            std::fs::write(path, csv_string)?;
        }

        // 转换成功，返回 `Ok(())`
        Ok(())
    }
}
#[cfg(feature = "xlsx")]
pub use xlsx::*;
