#[allow(warnings)]
#[cfg(feature = "json")]
pub mod json{
    pub use serde_json::json;
               ///ValueToBytes
            ///```ignore
            ///let data = json!({
            ///    "user_id": 89579,
            ///    "auth_password": "dW5kZWZpbmVk",
            ///    "blockchain": false,
            ///    "blockchain_token_all": 10000,
            ///    "blockchain_init_token": null
            ///});
            ///let json_bytes:Vec<u8> = data.as_bytes();
            /// ```
            pub trait ValueToBytes {
                ///ValueToBytes
                ///```ignore
                ///let data = json!({
                ///    "user_id": 89579,
                ///    "auth_password": "dW5kZWZpbmVk",
                ///    "blockchain": false,
                ///    "blockchain_token_all": 10000,
                ///    "blockchain_init_token": null
                ///});
                ///let json_bytes:Vec<u8> = data.as_bytes();
                /// ```
            fn as_bytes(&self)->Vec<u8>;
        }
        impl ValueToBytes for serde_json::Value {
            fn as_bytes(&self)->Vec<u8> {
                serde_json::to_vec(self)
                .expect("Failed to serialize data passed to send_json into JSON")
            }
        }
         ///ToJsonValue
            /// ```ignore
            /// let r = r#"
            /// {
            ///     "user_id": 89579,
            ///     "auth_password": "dW5kZWZpbmVk",
            ///     "blockchain": false,
            ///     "blockchain_token_all": 10000,
            ///     "blockchain_init_token": null
            /// }
            /// "#.trim();
            /// let value = r.to_json_value();
            /// ```
        pub trait ToJsonValue {
            ///ToJsonValue
            /// ```ignore
            /// let r = r#"
            /// {
            ///     "user_id": 89579,
            ///     "auth_password": "dW5kZWZpbmVk",
            ///     "blockchain": false,
            ///     "blockchain_token_all": 10000,
            ///     "blockchain_init_token": null
            /// }
            /// "#.trim();
            /// let value = r.to_json_value();
            /// ```
            fn to_json_value(&self) -> serde_json::Value;
        }
    
        impl<T:ToString> ToJsonValue for T {
            fn to_json_value(&self) -> serde_json::Value {
                serde_json::from_slice(&self.to_string().as_bytes()).unwrap()
            }
        }
}

#[cfg(feature = "json")]
pub use json::*;
