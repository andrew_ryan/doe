/// logger mod
/// ```ignore
/// fn main() {
///     use doe::logger::*;
///     doe::logger::init();
///     debug!("hello world");
///     info!("hello world");
///     warn!("hello world");
/// }
/// ```
///
#[allow(warnings)]
#[cfg(feature = "logger")]
pub mod logger {
    pub use tracing::*;
    pub fn init_debug() {
        // 导入tracing_subscriber库中的模块，用于日志过滤和格式化
        use tracing_subscriber::{filter::EnvFilter, fmt, layer::SubscriberExt, Registry};

        // 尝试从环境变量中解析日志级别过滤器，如果失败则默认为"info"级别
        let env_filter =
            EnvFilter::try_from_default_env().unwrap_or_else(|_| EnvFilter::new("debug"));
        use chrono::Local;
        use time::UtcOffset;
        use tracing_subscriber::fmt::time::OffsetTime;

        let offset = UtcOffset::current_local_offset().expect("should get local offset!");
        let time_format =
            time::format_description::parse("[year]-[month]-[day] [hour]:[minute]:[second]")
                .expect("format string should be valid!");
        let timer = OffsetTime::new(offset, time_format);
        // 创建一个格式化日志层，使用本地时间格式化日志，并将日志输出到标准错误流
        let formatting_layer = fmt::layer()
            .with_timer(timer.clone())
            .pretty()
            .with_writer(std::io::stdout);

        // 导入滚动日志文件追加器，用于创建日志文件
        use tracing_appender::rolling::{RollingFileAppender, Rotation};

        // 创建一个滚动日志文件追加器，日志文件名基于日期旋转，但在这里设置为从不旋转
        let file_appender = RollingFileAppender::builder()
            .rotation(Rotation::DAILY)
            .filename_prefix("app")
            .filename_suffix("log")
            .build("log")
            .expect("initializing rolling file appender failed");

        let stdout = std::io::stdout.with_max_level(tracing::Level::INFO);

        // 创建一个文件日志层，禁用ANSI颜色，并使用非阻塞模式写入日志文件
        let file_layer = fmt::layer()
            .with_ansi(false)
            .with_timer(timer)
            .with_writer(file_appender);

        // 将默认的Registry与环境变量过滤器、格式化日志层和文件日志层组合，并初始化
        let registy = Registry::default()
            .with(env_filter)
            .with(formatting_layer)
            .with(file_layer);
        registy.max_level_hint();
        let _ = tracing::subscriber::set_global_default(registy);
    }


    pub fn init_info() {
        use tracing_subscriber::{filter::EnvFilter, fmt, layer::SubscriberExt, Registry};
        use chrono::Local;
        use time::UtcOffset;
        use tracing_subscriber::fmt::time::OffsetTime;
    
        // 从环境变量中解析日志级别过滤器，默认为 "info"
        let env_filter =
            EnvFilter::try_from_default_env().unwrap_or_else(|_| EnvFilter::new("info"));
    
        // 设置本地时间格式
        let offset = UtcOffset::current_local_offset().expect("should get local offset!");
        let time_format =
            time::format_description::parse("[year]-[month]-[day] [hour]:[minute]:[second]")
                .expect("format string should be valid!");
        let timer = OffsetTime::new(offset, time_format);
    
        // 创建一个滚动日志文件追加器
        use tracing_appender::rolling::{RollingFileAppender, Rotation};
        let file_appender = RollingFileAppender::builder()
            .rotation(Rotation::DAILY)
            .filename_prefix("app")
            .filename_suffix("log")
            .build("log")
            .expect("initializing rolling file appender failed");
    
        // 创建一个文件日志层，禁用ANSI颜色，并使用非阻塞模式写入日志文件
        let file_layer = fmt::layer()
            .with_ansi(false)
            .with_timer(timer.clone())
            .with_writer(file_appender);
        let formatting_layer = fmt::layer()
        .with_timer(timer.clone())
        .pretty()
        .with_writer(std::io::stdout);

    
        // 将默认的Registry与环境变量过滤器、控制台日志层和文件日志层组合，并初始化
        // 将默认的Registry与环境变量过滤器、格式化日志层和文件日志层组合，并初始化
        let registy = Registry::default()
            .with(env_filter)
            .with(formatting_layer)
            .with(file_layer);
        registy.max_level_hint();
        let _ = tracing::subscriber::set_global_default(registy);
    
    }
    use tracing_appender::{non_blocking, rolling};
    use tracing_error::ErrorLayer;
    use tracing_subscriber::fmt::writer::MakeWriterExt;
    use tracing_subscriber::{
        filter::EnvFilter, fmt, layer::SubscriberExt, util::SubscriberInitExt, Registry,
    };

    /// Initializes the logger with the given configuration.
    ///
    /// # Arguments
    ///
    /// * `log_level` - The log level to use. Defaults to "info" if not provided.
    /// * `log_file_path` - The path to the log file. If `None`, logs will only go to stdout.
    /// * `pretty_print` - Whether to use pretty printing for logs. Defaults to `false`.
    ///
    /// # Example
    ///
    /// ```rust
    /// logger::init_with_option(Some("debug"), Some("logs/app.log"));
    /// ```
    pub fn init_with_option(log_level: Option<&str>, log_file_path: Option<&str>) {
        let env_filter = EnvFilter::try_from_default_env()
            .unwrap_or_else(|_| EnvFilter::new(log_level.unwrap_or("info")));

        // Create a formatting layer for stderr
        let formatting_layer = fmt::layer()
            .with_timer(tracing_subscriber::fmt::time::time())
            .with_level(true)
            .with_file(true)
            .with_writer(std::io::stderr);

        let formatting_layer = formatting_layer.pretty();

        let subscriber = Registry::default()
            .with(env_filter)
            .with(ErrorLayer::default())
            .with(formatting_layer);

        if let Some(path) = log_file_path {
            let file_appender = rolling::never("logs", path);
            let (non_blocking_appender, guard) = non_blocking(file_appender);

            let file_layer = fmt::layer()
                .with_ansi(false)
                .with_writer(non_blocking_appender);

            let subscriber = subscriber.with(file_layer);

            // Initialize the subscriber
            tracing::subscriber::set_global_default(subscriber)
                .expect("Failed to set global subscriber");

            // Ensure all logs are flushed before the application exits
            std::mem::drop(guard);
        } else {
            // Initialize the subscriber without file logging
            tracing::subscriber::set_global_default(subscriber)
                .expect("Failed to set global subscriber");
        }
    }
}
#[cfg(feature = "logger")]
pub use logger::*;
