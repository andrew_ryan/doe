/// Asynchronous utilities for working with Tokio runtime and async operations
/// 
/// Provides functionality for:
/// - Blocking on async operations
/// - Running system commands asynchronously
/// 
/// # Examples
/// 
/// ```ignore
/// #[allow(warnings)]
/// fn main() {
/// // Block on an async operation
/// asyncrs::block_on(async {
///     let output = asyncrs::command("echo hello").await;
///     println!("{}", String::from_utf8_lossy(&output.stdout));
/// });
///     let rt = doe::asyncrs::runtime::Builder::new_multi_thread()
///         .enable_all()
///         .build()
///         .unwrap();
///     rt.block_on(run());
///     rt.block_on(async {
///         run().await;
///     });
/// }
/// 
/// async fn run() {}
/// 
/// ```
#[allow(warnings)]
#[cfg(feature = "asyncrs")]
pub mod asyncrs {
    pub use tokio::runtime::Runtime;
    pub use tokio::*;

    use crate::Vector;

    /// Trait for blocking on async operations
    /// 
    /// Provides a method to block the current thread until a future completes
    pub trait BlockOn {
        /// Blocks the current thread until the provided future completes
        /// 
        /// # Arguments
        /// 
        /// * `future` - A future that will be blocked on
        /// 
        /// # Panics
        /// 
        /// Panics if unable to create a new Tokio runtime
        /// 
        /// # Example
        /// 
        /// ```
        /// use doe::asyncrs::BlockOn;
        /// 
        /// ().block_on(async {
        ///     println!("Running async operation");
        /// });
        /// ```
        fn block_on<F>(&self, future: F)
        where
            F: std::future::Future<Output = ()> + Send + 'static;
    }

    impl BlockOn for () {
        fn block_on<F>(&self, future: F)
        where
            F: std::future::Future<Output = ()> + Send + 'static,
        {
            let rt = tokio::runtime::Runtime::new().unwrap();
            rt.block_on(future);
        }
    }

    /// Blocks the current thread on the given future, executing it to completion
    /// 
    /// Creates a new Tokio runtime and uses it to run the provided future
    /// 
    /// # Arguments
    /// 
    /// * `future` - A future that will be blocked on
    /// 
    /// # Panics
    /// 
    /// Panics if unable to create a new Tokio runtime
    pub fn block_on<F>(future: F)
    where
        F: std::future::Future<Output = ()> + Send + 'static,
    {
        let rt = tokio::runtime::Runtime::new().unwrap();
        rt.block_on(future);
    }

    /// Executes a system command asynchronously
    /// 
    /// # Arguments
    /// 
    /// * `command` - The command to execute, can be a string or byte slice
    /// 
    /// # Returns
    /// 
    /// Returns a `std::process::Output` containing the command's output
    /// 
    /// # Panics
    /// 
    /// Panics if unable to spawn the child process
    /// 
    /// # Example
    /// 
    /// ```
    /// use doe::asyncrs;
    /// 
    /// asyncrs::block_on(async {
    ///     let output = asyncrs::command("echo hello").await;
    ///     println!("{}", String::from_utf8_lossy(&output.stdout));
    /// });
    /// ```
    pub async fn command(command: impl AsRef<[u8]>) -> std::process::Output {
        use std::process::Stdio;
        use tokio::process::Command;
        let command = command.as_ref().to_string_lossy().to_string();
        let exe = command.split(" ").into_iter().nth(0).unwrap();
        let other = command.split(" ").into_iter().skip(1).collect::<Vec<_>>();
        let child = Command::new(exe)
            .args(other)
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .spawn()
            .expect("Failed to spawn child process");
        let output = child
            .wait_with_output()
            .await
            .expect("Failed to wait on child");
        output
    }
}
#[cfg(feature = "asyncrs")]
pub use asyncrs::*;
