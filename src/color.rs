/// Color conversion utilities
/// 
/// Provides functions for converting between hex color codes and RGB values
/// 
/// # Examples
/// 
/// ```ignore
/// use doe::color;
/// 
/// // Convert hex to RGB
/// let rgb = color::hex_to_rgb("#FF5733").unwrap();
/// assert_eq!(rgb, (255, 87, 51));
/// 
/// // Convert RGB to hex
/// let hex = color::rgb_to_hex((255, 87, 51));
/// assert_eq!(hex, "#FF5733");
/// ```
#[allow(warnings)]
pub mod color {
    /// Converts a hex color string to RGB values
    /// 
    /// # Arguments
    /// 
    /// * `hex` - A string slice containing a hex color code (e.g. "#FF5733")
    /// 
    /// # Returns
    /// 
    /// Result containing a tuple of (red, green, blue) values or an error string
    /// 
    /// # Example
    /// 
    /// ```ignore
    /// use doe::color;
    /// 
    /// match color::hex_to_rgb("#FF5733") {
    ///     Ok((r, g, b)) => println!("Red: {}, Green: {}, Blue: {}", r, g, b),
    ///     Err(e) => eprintln!("Error: {}", e),
    /// }
    /// ```
    pub fn hex_to_rgb(hex: &str) -> Result<(u8, u8, u8), String> {
        if hex.len() != 7 || !hex.starts_with('#') {
            return Err("Invalid hex color format. Expected format: #RRGGBB".to_string());
        }

        let r = u8::from_str_radix(&hex[1..3], 16).map_err(|e| e.to_string())?;
        let g = u8::from_str_radix(&hex[3..5], 16).map_err(|e| e.to_string())?;
        let b = u8::from_str_radix(&hex[5..7], 16).map_err(|e| e.to_string())?;

        Ok((r, g, b))
    }

    /// Converts RGB values to a hex color string
    /// 
    /// # Arguments
    /// 
    /// * `rgb` - A tuple of (red, green, blue) values (0-255)
    /// 
    /// # Returns
    /// 
    /// String containing the hex color code (e.g. "#FF5733")
    /// 
    /// # Example
    /// 
    /// ```ignore
    /// use doe::color;
    /// 
    /// let hex = color::rgb_to_hex((255, 87, 51));
    /// assert_eq!(hex, "#FF5733");
    /// ```
    pub fn rgb_to_hex(rgb: (u8, u8, u8)) -> String {
        format!("#{:02X}{:02X}{:02X}", rgb.0, rgb.1, rgb.2)
    }
}
