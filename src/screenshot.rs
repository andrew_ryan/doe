#[allow(warnings)]
#[cfg(feature = "screenshot")]
pub mod screenshot {
    use image::{GenericImageView, ImageBuffer, Rgba, RgbaImage};
    use crate::images;

    /// Captures screenshots of all available screens and saves them as PNG files.
    ///
    /// The screenshots are saved with filenames in the format `screenshot_{index}.png`,
    /// where `index` corresponds to the screen number.
    ///
    /// # Examples
    ///
    /// ```
    /// use doe::screenshot;
    /// screenshot::capture_all();
    /// ```
    pub fn capture_all() {
        use screenshots::Screen;
        let screens = Screen::all().unwrap();
        for (index, screen) in screens.iter().enumerate() {
            let mut image = screen.capture().unwrap();
            image.save(format!("screenshot_{}.png", index)).unwrap();
        }
    }

    /// Crops a specific area from an image.
    ///
    /// # Arguments
    ///
    /// * `img` - The source image as an `ImageBuffer<Rgba<u8>, Vec<u8>>`.
    /// * `x` - The x-coordinate of the top-left corner of the area to crop.
    /// * `y` - The y-coordinate of the top-left corner of the area to crop.
    /// * `width` - The width of the area to crop.
    /// * `height` - The height of the area to crop.
    ///
    /// # Returns
    ///
    /// Returns `Some(RgbaImage)` containing the cropped area if successful.
    /// Returns `None` if the cropping operation fails.
    ///
    /// # Examples
    ///
    /// ```
    /// use doe::screenshot;
    /// use image::{ImageBuffer, Rgba};
    ///
    /// let img = ImageBuffer::new(100, 100);
    /// let cropped = screenshot::cut_area(img, 10, 10, 50, 50).unwrap();
    /// ```
    pub fn cut_area(img: ImageBuffer<Rgba<u8>, Vec<u8>>, x: u32, y: u32, width: u32, height: u32) -> Option<RgbaImage> {
        let sub_image = img.view(x, y, width, height);
        let mut cut_image = RgbaImage::new(width, height);
        for (x, y, pixel) in sub_image.pixels() {
            cut_image.put_pixel(x, y, pixel);
        }
        Some(cut_image)
    }

    /// Captures a specific area from the primary screen.
    ///
    /// # Arguments
    ///
    /// * `x` - The x-coordinate of the top-left corner of the area to capture.
    /// * `y` - The y-coordinate of the top-left corner of the area to capture.
    /// * `width` - The width of the area to capture.
    /// * `height` - The height of the area to capture.
    ///
    /// # Returns
    ///
    /// Returns `Some(RgbaImage)` containing the captured area if successful.
    /// Returns `None` if the capture operation fails.
    ///
    /// # Examples
    ///
    /// ```
    /// use doe::screenshot;
    ///
    /// let captured = screenshot::capture_area(10, 10, 50, 50).unwrap();
    /// ```
    pub fn capture_area(x: u32, y: u32, width: u32, height: u32) -> Option<RgbaImage> {
        use screenshots::Screen;
        let screens = Screen::all().unwrap();
        if let Some(screen) = screens.iter().nth(0) {
            let image = screen.capture().unwrap();
            let new_image = cut_area(image, x, y, width, height);
            return new_image;
        } else {
            return None;
        }
    }

    /// Retrieves the RGB color value from a specific position in an image.
    ///
    /// # Arguments
    ///
    /// * `image_path` - The path to the image file.
    /// * `x` - The x-coordinate of the pixel to retrieve.
    /// * `y` - The y-coordinate of the pixel to retrieve.
    ///
    /// # Returns
    ///
    /// Returns `Some((u8, u8, u8))` containing the RGB values if the position is valid.
    /// Returns `None` if the position is outside the image bounds.
    ///
    /// # Examples
    ///
    /// ```
    /// use doe::screenshot;
    ///
    /// let rgb = screenshot::get_rgb_from_position("screenshot_0.png", 10, 10).unwrap();
    /// println!("RGB: {:?}", rgb);
    /// ```
    pub fn get_rgb_from_position(image_path: &str, x: u32, y: u32) -> Option<(u8, u8, u8)> {
        use image::{GenericImageView, ImageBuffer, Rgb};
        let image = image::open(image_path).expect("Failed to open image");
        let width = image.width();
        let height = image.height();
        if x < width && y < height {
            let pixel = image.get_pixel(x, y);
            let r = pixel[0];
            let g = pixel[1];
            let b = pixel[2];
            let a = pixel[3];
            return Some((r, g, b));
        } else {
            return None;
        }
    }

    /// Checks if the color at a specific screen position matches a given hex color.
    ///
    /// # Arguments
    ///
    /// * `position` - A tuple `(u32, u32)` representing the screen position to check.
    /// * `hex` - A string slice representing the hex color to compare against.
    ///
    /// # Returns
    ///
    /// Returns `true` if the color at the specified position matches the hex color.
    /// Returns `false` otherwise.
    ///
    /// # Examples
    ///
    /// ```
    /// use doe::screenshot;
    ///
    /// let is_color_match = screenshot::screen_position_is((10, 10), "#FFFFFF");
    /// println!("Color match: {}", is_color_match);
    /// ```
    pub fn screen_position_is(position: (u32, u32), hex: &str) -> bool {
        capture_all();
        if crate::color::color::rgb_to_hex(
            get_rgb_from_position("screenshot_0.png", position.0, position.1).unwrap(),
        ) == hex
        {
            true
        } else {
            false
        }
    }
}

#[cfg(feature = "screenshot")]
pub use screenshot::*;